# Задание 2. Движущая картинка в консоли.

import time

mas = []

file = open('earth.md', 'r')
string = ''.join([line for line in file])  # Получение всех символов
num = 1  # Номер кадра

while num < 26:
    frame = string.split(f'\n```\n')  # Разделение кадров
    mas.append(str(frame[num]))  # Добавление кадров в общий массив
    num += 1

print('\n' * 100)

# Вывод анимации
for j in range(1, 3):
    for i in range(0, len(mas)):
        if i % 2 == 0:
            print(f'\033[96m{mas[i]}\033[0m')  # Вывод цветных кадров
            time.sleep(1)
            print('\n' * 100)

file.close()

