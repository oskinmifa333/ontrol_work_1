# Задание_1. Вычислить сумму последовательности: 42 + 1/x_1 + 1/x_2 ..

const = 42
task_1 = open("task_1.txt") # Работа с файлом

# Сложение строк из файла
while True:
    line = task_1.readline()  # Получение строки
    if not line:
        break

    const += 1 / float(line)  # Вычисление

task_1.close()
print(f'Результат вычисления: {const}')

