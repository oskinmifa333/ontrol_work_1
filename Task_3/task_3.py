# Реализация алгоритмов SAXPY.
# Постройте график роста времени вычисления от длины вектора для целочисленой и вещественной реализации алгоритма
import matplotlib.pyplot as plt
import time

x = y = a = vector = sec = []
result = [[], []]
timing = 0
n = input('Введите длину значений x, y, константу a(через пробел): ').split()


def gen_lst(lst):
    global n
    for i in range(int(n[0])):
        lst.append(i)
    return lst


gen_lst(x)
gen_lst(y)


def saxpy(res):  # Реализация SAXPY
    global n, timing, x, y
    for i in range(len(x)):
        start_timing = time.perf_counter()
        vec = int(n[1]) * int(x[i]) + int(y[i])
        timing = time.perf_counter() - start_timing
        res[0].append(vec)
        res[1].append(timing)
    return res


fig, ax = plt.subplots()

ax.plot(result[0], result[1])
plt.show()
