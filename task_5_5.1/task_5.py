# Алгоритм решения уравнения методом Краммера
import memory_profiler
from memory_profiler import profile
systEquat = [[1, 2, 1, 1], [2, 1, 0, 0], [0, 0, 1, 0]]  # Система уравнений
timing = str
# Нахождения делителя матрицы


@profile
def det(mat):
    res = 1  # Конечный результат
    matLength = len(mat)  # Длина матрицы

    # Получение делителя
    for i in range(matLength):
        res *= mat[i][i]

        # Получение минора матрицы
        for j in range(i + 1, matLength):
            b = mat[j][i] / mat[i][i]
            mat[j] = [mat[j][k] - b * mat[i][k] for k in range(matLength)]

    return int(res)


mainDelta = []  # Главный определитель системы
tempMatx = []  # Строка главного определителя системы

delta = []  # Определитель системы
matxX = []  # Главный столбец матрицы
tempMatx2 = []  # Строка определителя матрицы
count = 0  # Номер столбца определителя

# Нахождение главного определителя
for i in range(0, len(systEquat)):
    for j in range(0, len(systEquat[i]) - 1):
        tempMatx.append(systEquat[i][j])  # Получение строк главного определителя

    mainDelta.append(tempMatx)  # Добавление строки в главный определитель
    tempMatx = []

# Нахождение определителя
for z in range(0, len(systEquat)):
    for i in range(0, len(systEquat)):
        matxX.append(systEquat[i][len(systEquat)])  # Нахождение последних чисел списка

        for j in range(0, len(systEquat[i])):
            if j == count:
                tempMatx2.append(matxX[i])  # Добавление главного столбца определителя
                continue

            tempMatx2.append(systEquat[i][j])  # Добавление чисел во временный массив

        delta.append(tempMatx2)  # Добавление строки в определитель
        tempMatx2 = []

    count += 1  # Увеличение номера столбца

    print(det(delta) / det(mainDelta))  # Вычисление определителя
    delta = []
